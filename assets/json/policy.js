const data = {
  policy1: `<p>주식회사 엠에스커뮤니케이션은 개인정보보호법, 신용정보의 이용 및 보호에 관한 법률, 정보통신망 이용촉진 및 정보보호 등에 관한 법률 등 개인정보처리자, 정보통신 서비스 제공자가 준수하여야 할 관련 법령상의 개인정보보호 규정을 준수하며, 관련 법령에 의거한 개인정보처리방침을 정하여 회원 권익 보호에 최선을 다하고 있습니다.&nbsp;</p>
        <p><br></p>
        <p>주식회사 엠에스커뮤니케이션(이하 &ldquo;회사&rdquo;)은 개인정보취급방침을 제정하여 이를 준수하고 있으며, 본 취급방침을 공개하여 언제나 쉽게 열람하실 수 있도록 하고 있습니다.</p>
        <p>(제목 및 서문)</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보 수집&middot;이용 목적</p>
        <p>회사는 수집한 개인정보를 다음의 목적을 이용해 활용합니다.</p>
        <p>&nbsp; &nbsp; &middot; 서비스 제공을 위한 회사명 확인 및 본인 확인</p>
        <p>&nbsp; &nbsp; &middot; 세비스 제공을 위한 예약사항 전달 및 상담</p>
        <p>&nbsp; &nbsp; &middot; 각종 혜택 안내</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;수집하려는 개인정보의 항목 및 수집방법</p>
        <p>회사가 서비스 제공을 위해 제공받는 개인 정보는 아래와 같으며, 고객 스스로 제공하는 정보 또는 사이트 접속 시 자동으로 기록되는 정보를 뜻합니다.</p>
        <p>&nbsp; &nbsp; &middot; 이름, 휴대전화 번호, 이메일 주소, 회사명, 창업여부, 창업 단계</p>
        <p><br></p>
        <p>[사이트 접속시 수집될 수 있는 개인정보] 쿠키 정보</p>
        <p>&middot; 회사는 홈페이지, 앱등 서비스 이용자에게 개별 맞춤 서비스를 제공하기 위해 이용 정보를 저장하고 수시로 불러오는 &lsquo;쿠키(Cookie)&rsquo;를 사용합니다. 이용자가 웹사이트에 접속할 경우 웹 사이트 서버는 이용자의 디바이스에 저장되어 있는 쿠키의 내용을 읽어 이용자의 환경설정을 유지하고 맞춤 서비스를 제공하게 됩니다. 이용자는 브라우저의 설정을 통해 쿠키 저장을 거부하거나 쿠키를 삭제할 수 있습니다. 다만, 쿠키 저장을 거부할 경우, 필요한 서비스 이용에 어려움을 겪을 수 있습니다.</p>
        <p>(1) 쿠키의 사용목적</p>
        <p>&middot; 쿠키는 이용자가 웹 사이트를 방문할 때, 웹 사이트 사용을 설정한대로 접속하고 편리하게 사용할 수 있도록 합니다. 또한, 이용자의 웹사이트 방문 기록, 이용 형태를 통해서 최적화된 광고 등 맞춤형 정보를 제공하기 위해 활용됩니다.</p>
        <p>(2) 쿠키 설정 거부 방법</p>
        <p>웹 브라우저</p>
        <p>&middot; Chrome: 메뉴 &gt; 설정 &gt; &lsquo;개인정보 및 보안&rsquo;</p>
        <p>&middot; Edge: 설정 및 기타 항목 &gt; 설정 &gt; 사이트 사용 권한 &gt; 쿠키 및 사이트 데이터</p>
        <p>&middot; Safari: 환경설정 &gt; &lsquo;크로스 사이트 추적 방지&rsquo; 및 &lsquo;모든 쿠키 차단&rsquo;</p>
        <p>이 외에도 Firefox, Opera 등 주요 인터넷 웹브라우저들도 쿠키 삭제 기능을 제공하고 있습니다.</p>
        <p>모바일</p>
        <p>&middot; Android: 홈 &gt; 설정 &gt; Google &gt; 광고 &gt; 광고 맞춤설정 선택 해제 (ON)</p>
        <p>&middot; iPhone: 홈 &gt; 설정 &gt; 개인 정보 보호 &gt; 추적 &gt; 앱이 추적을 요청하도록 허용 (OFF)</p>
        <p><br></p>
        <p>2. 수집 방법</p>
        <p>&middot; MS커뮤니케이션 상담 예약 및 회사 공식 이메일과 고객센터를 통한 온라인 상담</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보의 처리 및 보유기간</p>
        <p>고객님의 개인정보는 법령에 따른 개인정보 보유&middot;이용기간 또는 회사가 고객으로부터 개인정보를 수집사에 동의받은 개인정보 보유&middot;이용기간 내에서 처리 및 보유됩니다.</p>
        <p>&nbsp; &nbsp; &middot; 서비스 상담 예약 : 정보 제출일로부터 2년</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보의 제3자 제공에 관한 사항</p>
        <p>회사와 서비스 제공 계약을 체결한 고객에게 더 질 높은 서비스를 제공하기 위하여 관련 계약 및 서비스를 동의한 경우에 한하여 아래와 같이 고객의 정보를 제공하고 있습니다.</p>
        <p>⦁ &nbsp; &nbsp;제공정보의 이용목적 : 제휴 마케팅, 혜택 서비스 제공, 고객상담 및 불만처리</p>
        <p>&middot; 제공대상&nbsp;</p>
        <p>⦁ &nbsp; &nbsp;주식회사 씨와이 : 제휴 마케팅 및 혜택 서비스 제공, 고객상담 및 불만처리,&nbsp;</p>
        <p>⦁ &nbsp; &nbsp;각 제휴 혜택사 : 혜택 서비스 제공, 고객상담 및 불만처리</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보의 파기절차 및 방법</p>
        <p>회사는 원칙적으로 개인정보의 수집 및 이용목적이 달성된 후 해당 정보를 지체 없이 파기합니다. 파기 절차 및 방법은 다음과 같습니다.</p>
        <p>⦁ &nbsp; &nbsp;파기절차&nbsp;</p>
        <p>⦁ &nbsp; &nbsp;원칙적으로 고객이 서비스 가입 등을 위해 제공한 정보는 수집 및 이용목적이 달성된 후 내부방침 및 관련 법령에 의한 보유사유에 따라 일정기간 저장된 후 파기됩니다.</p>
        <p>⦁ &nbsp; &nbsp;별도 DB로 옮겨진 개인정보는 법률에 의한 경우가 아니면 보유되는 목적 이외의 목적으로 이용되지 않습니다.</p>
        <p>⦁ &nbsp; &nbsp;파기대상 : 보유기간 및 관련 법령에 따른 보존기간이 종료된 고객정보</p>
        <p>⦁ &nbsp; &nbsp;파기방법</p>
        <p>⦁ &nbsp; &nbsp;종이(서면)에 작성 출력된 개인정보 : 분쇄 및 소각의 방법으로 파기</p>
        <p>⦁ &nbsp; &nbsp;DB 등 전자 파일 형태로 저장된 개인 정보 : 재생할 수 없는 기술적 방법으로 삭제</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보 취급의 위탁</p>
        <p>회사는 정보제공자의 동의없이 개인정보 취급을 외부 업체에 위탁하지 않습니다. 향후 필요에 의해 위탁할 경우, 위탁 대상자와 업무 내용에 대해 사전 고지 및 동의를 받겠습니다.</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;정보주체의 권리, 의무 및 그 행사방법</p>
        <p>고객(만 14세 미만일 경우 법정대리인 포함)이 개인정보에 대한 열람 및 정정을 요구하시거나 개인정보의 수집과 이용, 위탁 또는 제공에 대한 동의를 철회할 수 있습니다.</p>
        <p>⦁ &nbsp; &nbsp;개인정보의 열람, 증명 또는 정정</p>
        <p>가. 고객이 회사를 방문하시어 개인정보에 대한 열람 증명을 요구 할 수 있습니다. 그러나 고객님의 개인정보 보호를 위하여 전화, 우편, Fax 등의 신청방법에 의해서는 고객의 개인정보에 대한 열람, 증명이 제공 되지는 않습니다.</p>
        <p>나. 고객님께서 본인의 개인정보에 대한 열람, 증명을 요구하시는 경우 고객님의 신분을 증명 할 수 있는 주민 등록증, 여권, 운전면허증(신형) 등의 신분증명(사본)을 제시 받아 본인 여부를 확인합니다.</p>
        <p>다. 고객의 대리인이 열람증명을 요구하는 경우에는 대리 관계를 나타내는 위임장, 명의고객님의 인감증명서와 대리인의 신분증명서 등의 증표를 제시 받아 대리인인지 여부를 확인합니다.</p>
        <p>라. 고객의 개인정보는 오류에 대한 정정을 요청하신 경우에는 정정을 완료하기 전까지 당해 개인정보를 이용 또는 제공하지 않습니다. 또한 잘못된 개인정보를 제3자에게 이미 제공한 경우에는 정정 처리결과를 제3자에게 지체 없이 통지하여 정정이 이루어지도록 하겠습니다.</p>
        <p><br></p>
        <p>2. 개인정보의 수집과 이용 또는 제공에 대한 동의 철회</p>
        <p>가. 고객이 위 제 1항 &apos;나&apos;에 고지된 신분증을 지참하시고 회사를 방문하여 개인정보의 수집과 이용, 위탁 또는 제공에 대해 동의 하시거나, 이에 대한 동의를 선택적으로 철회하실 수 있습니다.</p>
        <p>나. 회사의 고객센터에 전화 또는 &apos;사이버고객센터 E-mail 상담&apos;등으로 연락하시면 지체 없이 조치하겠습니다.</p>
        <p><br></p>
        <p>※ 단, 서비스 제공에 필요한 필수정보의 수집과 이용에 관한 동의철회는 예외로 합니다</p>
        <p><br></p>
        <p>다. 법정대리인(부모 중 어느 일방이 요구할 경우에도 가능)은 만 14세미만 아동의 개인정보 수집과 이용 또는 제공에 대한 동의를 철회할 수 있으며, 만 14세 미만의 아동이 제공한 개인정보에 대한 열람 또는 오류의 정정을 요구할 수 있습니다.</p>
        <p>라. 회사는 고객님의 요청에 의해 해지 또는 삭제된 개인정보는 &ldquo;회사가 수집하는 개인정보의 보유 및 이용기간&rdquo;에 명시된 바에 따라 처리하고 그 외의 용도로 열람 또는 이용할 수 없도록 처리하고 있습니다.</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보의 안전성 확보조치에 관한 사항</p>
        <p>회사는 고객의 개인정보를 처리함에 있어 개인정보가 분실, 도난, 유출, 변조 또는 훼손되지 않도록 안전성 확보를 위하여 다음과 같은 관리적, 기술적 대책을 강구하고 있습니다.</p>
        <p>&nbsp; &nbsp; &middot; 관리적 조치 : 내부관리 계획 수립 및 시행, 전 직원에 대한 정기적인 정보보호 교육</p>
        <p>&nbsp; &nbsp; &middot; 기술적 조치 : 개인정보처리시스템의 접근 권한 관리, 접근통제시스템 설치, 보안 프로그램 설치</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;정보주체와 법정대리인의 권리&middot;의무 및 행사벙법에 관한 사항</p>
        <p>1. 정보주체(만 14세 미만인 경우에는 법정대리인을 말함)는 언제든지 다음 각 호의 개인정보 보호 관련 권리를 행사할 수 있습니다.</p>
        <p><br></p>
        <p>1) 개인정보 열람요구</p>
        <p>2) 오류 등이 있을 경우 정정 요구</p>
        <p>3) 삭제요구</p>
        <p>4) 처리정지 요구</p>
        <p>5) 개인정보 수집, 이용, 제공 등의 동의 철회요구</p>
        <p>2. 제1항에 따른 권리 행사는 회사에 대해 서면, 전화, 전자우편, 모사전송(FAX) 등을 통하여 하실 수 있으며 회사는 이에 대해 지체없이 조치하겠습니다.</p>
        <p><br></p>
        <p>3. 정보주체가 개인정보의 오류 등에 대한 정정 또는 삭제를 요구한 경우에는 회사는 정정 또는 삭제를 완료할 때까지 당해 개인정보를 이용하거나 제공하지 않습니다.</p>
        <p><br></p>
        <p>4. 제1항에 따른 권리 행사는 정보주체의 법정대리인이나 위임을 받은 자 등 대리인을 통하여 하실 수 있습니다. 이 경우 개인정보 보호법 시행규칙 별지 제11호 서식에 따른 위임장을 제출하셔야 합니다.</p>
        <p><br></p>
        <p>5. 정보주체는 개인정보 보호법 등 관계법령을 위반하여 회사가 처리하고 있는 정보주체 본인이나 타인의 개인정보 및 사생활을 침해하여서는 아니됩니다.</p>
        <p><br></p>
        <p>6. 회사는 정보주체의 청구에 따라 CCTV의 영상 등을 제공하는데 있어서 타인의 개인정보를 보호하기 위한 모자이크 처리 등에 소요되는 비용을 청구할 수 있습니다.</p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보 보호책임자</p>
        <p>1. 회사는 개인정보의 처리에 관한 업무를 총괄해서 책임질 개인정보 보호책임자를 지정하고 있습니다.</p>
        <p><br></p>
        <p>개인정보보호 책임자 : 홍석원</p>
        <p>2. 회사는 서비스 이용 도중 발생한 모든 개인정보보호 관련 문의, 불만 처리, 피해 구제 등의 사항이 원활히 처리될 수 있도록 담당 부서를 지정하고 있습니다.</p>
        <p><br></p>
        <p>부서명 : 운영팀</p>
        <p>연락처 : <a href="mailto:admin@mscomm.kr">admin@mscomm.kr</a></p>
        <p><br></p>
        <p>⦁ &nbsp; &nbsp;개인정보 처리 방침의 변경에 관한 사항</p>
        <p>회사는 개인정보 처리방침을 변경하는 경우에는 회사 인터넷 홈페이지에 관련 내용을 고지한 후 지속적으로 게시합니다.</p>
        <p><br></p>
        <p>본 &lt;개인정보 처리방침&gt;은 2023. 11. 01. 부터 적용됩니다.</p>
        <p><br></p>`,
};

export default data;
