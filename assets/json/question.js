const data = {
  question1: `<p><span style="text-align: start">1. 개인정보 이용 목적</span></p><br/>
  <p><span style="text-align: start">1) 엠에스커뮤니케이션 서비스 문의하기 신청에 따른 혜택 발송, 안내 및 관련 문의 사항 응대&nbsp;</span></p><br/>
  <p><span style="text-align: start">2) 이벤트 참여 내역 관리를 통한 혜택 중복수령 방지 등&nbsp;</span></p><br/>
  <p><span style="text-align: start">3) 광고성 정보 수신에 별도 동의한 자에 한하여 엠에스커뮤니케이션의 새로운 서비스 신상품이나 최신 정보 안내 등 문의자님의 필요에 맞는 최적의 서비스를 제공하기 위함&nbsp;</span></p><br/>
  <p><span style="text-align: start">2. 개인정보 이용 항목: 이름, 휴대폰번호, 회사명, 직함, 이메일&nbsp;</span></p><br/>
  <p><span style="text-align: start">3. 개인정보 보유/이용 기간: 수집한 개인정보는 서비스 문의하기 이용 후 1년간 보관합니다. 단, 서비스 문의하기 이후 별도 요청이 있을 시 즉시 파기합니다.&nbsp;</span></p><br/>
  <p><span style="text-align: start">4. 서비스 문의하기에 대한 개인정보 이용을 거부할 수 있습니다. 단, 거부의 경우에는 서비스 문의하기에 따른 1:1 상담 등의 서비스 이용이 제한됩니다.</span></p>`,
};

export default data;
