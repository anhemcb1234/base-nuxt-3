// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ["~/assets/css/tailwind.css", "~/assets/css/font.css"],
  fontMetrics: {
    fonts: [
      {
        family: "Roboto",
        fallbacks: ["Zapfino"],
        fallbackName: "fallback-a",
      },
      {
        family: "Roboto",
        fallbacks: ["Impact"],
        fallbackName: "fallback-b",
      },
      {
        family: "Roboto",
        fallbacks: ["Georgia"],
        fallbackName: "fallback-c",
      },
      {
        family: "Poppins",
        fallbacks: ["Georgia"],
        fallbackName: "fallback-poppins",
        src: "./assets/css/font/Pretendard_Font/Pretendard_Font/public/variable/PretendardVariable.ttf",
      },
    ],
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  app: {
    head: {
      meta: [
        {
          name: "viewport",
          content: "width=device-width, initial-scale=1",
        },
        {
          charset: "utf-8",
        },
        {
          name: "google-site-verification",
          content: "5ImpI7YPoxgVugd_ksJWTjtakD3yqbPh868bG36WsnM",
        },
      ],
      link: [
        {
          rel: "stylesheet",
          href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css",
        },
      ],
    },
  },
  runtimeConfig: {
    public: {
      apiBase:
        process.env.NODE_ENV == "prod"
          ? process.env.VITE_PROD || "/api"
          : process.env.VITE_SOME_KEY || "/api",
    },
    // API_KEY: process.env.NODE_ENV == 'prod' ? 'AAA' : 'BBB'
  },
  modules: [
    "@pinia/nuxt",
    "@vueuse/nuxt",
    "@invictus.codes/nuxt-vuetify",
    "@nuxt/image",
    "@vee-validate/nuxt",
    "@nuxt/ui",
    "@nuxtjs/fontaine",
    "@nuxtjs/robots",
    "@nuxtjs/color-mode",
  ],
  colorMode: {
    classSuffix: "",
  },
  vuetify: {
    /* vuetify options */
    vuetifyOptions: {
      // @TODO: list all vuetify options
    },

    moduleOptions: {
      /* nuxt-vuetify module options */
      treeshaking: false,
      useIconCDN: false,

      /* vite-plugin-vuetify options */
      styles: "none",
      autoImport: false,
      useVuetifyLabs: false,
    },
  },
  image: {
    domains: ["https://images.unsplash.com", "https://source.unsplash.com"],
    format: ["webp"],
    loading: "lazy",
    dir: "assets/image",
  },
  tailwindcss: {
    cssPath: "~/assets/css/tailwind.css",
    configPath: "tailwind.config.js",
    exposeConfig: false,
    exposeLevel: 2,
    config: {},
    injectPosition: "first",
    viewer: true,
  },
  components: {
    global: true,
    dirs: ["~/components/block"],
  },
  routeRules: {
    "/cms/**": { ssr: false },
  },
});
