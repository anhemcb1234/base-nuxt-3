import { Axios } from "./Axios";

const Login = (payload) => {
  return Axios.post(`/auth/sign-in`, payload);
};

const getListQuestions = (payload) => {
  return Axios.get(`/public/api/question`, payload);
};

const detailQuestion = ({ id }) => {
  return Axios.get(`/public/api/question/${id}`);
};

const changeQuestion = ({ id, payload }) => {
  return Axios.put(`/admin/api/question/${id}`, payload);
};

const getTotalQuestionByDate = () => {
  return Axios.get(`/admin/api/question/getTotalQuestionByDate`);
};

const exportExcel = (payload) => {
  return Axios.get(`/admin/api/question/getLinkExportExcel?ids=${payload}`);
};

const serviceCategory = (payload) => {
  return Axios.get(`/public/api/service-category`, payload);
};

const listServiceBpoCategory = (payload) => {
  return Axios.get(`/public/api/service`, payload);
};

const detailServiceBpo = ({ id }) => {
  return Axios.get(`/public/api/service/${id}`);
};

const getListService = (payload) => {
  return Axios.get(`/public/api/service`, payload);
};

const changeServiceBpo = ({ id, formData }) => {
  return Axios.put(`/admin/api/service/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

const delServiceBpo = ({ id }) => {
  return Axios.del(`/admin/api/service?ids=${id}`);
};

const createServiceBpo = ({ formData }) => {
  return Axios.post(`/admin/api/service`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

const getServiceAdminController = (payload) => {
  return Axios.get(`/public/api/service-landing`, payload);
};

const changeServiceAdminController = ({ id, payload }) => {
  return Axios.put(`/admin/api/service-landing/${id}`, payload);
};

const getListSeo = (payload) => {
  return Axios.get(`/public/api/page`, payload);
};

const getDetailListSeo = ({ id }) => {
  return Axios.get(`/public/api/page/${id}`);
};

const createSeoPage = ({ formData }) => {
  return Axios.post(`/admin/api/page`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

const changeSeoPage = ({ id, formData }) => {
  return Axios.put(`/admin/api/page/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

const delServiceSeo = ({ id }) => {
  return Axios.del(`/admin/api/page?ids=${id}`);
};

const getListDocuments = (payload) => {
  return Axios.get(`/public/api/document`, payload);
}

const getListDocumentID = (payload) => {
  return Axios.get(`/public/api/document/${payload}`);
}
const addDocument = (payload) => {
  return Axios.post(`/admin/api/document`, payload,{
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

const updateDocument = (id,payload) => {
  return Axios.put(`/admin/api/document/${id}`, payload,{
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

const delDocuments = (payload) => {
  return Axios.del(`/admin/api/document?ids=${payload.ids}`);
}

const getListBlogs = (payload) => {
  return Axios.get(`/public/api/blog`, payload);
}

const getListBlogID = (payload) => {
  return Axios.get(`/public/api/blog/${payload}`);
}
const addBlog = (payload) => {
  return Axios.post(`/admin/api/blog`, payload,{
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

const updateBlog = (id,payload) => {
  return Axios.put(`/admin/api/blog/${id}`, payload,{
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

const delBlogs = (payload) => {
  return Axios.del(`/admin/api/blog?ids=${payload.ids}`);
}

export const useCms = {
  Login,
  getListQuestions,
  getTotalQuestionByDate,
  exportExcel,
  detailQuestion,
  changeQuestion,
  serviceCategory,
  listServiceBpoCategory,
  detailServiceBpo,
  changeServiceBpo,
  delServiceBpo,
  createServiceBpo,
  changeServiceAdminController,
  getServiceAdminController,
  getListSeo,
  getDetailListSeo,
  createSeoPage,
  changeSeoPage,
  delServiceSeo,
  getListService,
  getListDocuments,
  delDocuments,
  addDocument,
  getListDocumentID,
  updateDocument,
  getListBlogs,
  getListBlogID,
  addBlog,
  updateBlog,
  delBlogs
};
