import axios from "axios";

let axiosInstance = null;
let headers = {
  "cache-control": "no-cache",
};

function setHeaders(inputHeaders) {
  headers = { headers, inputHeaders };
  getInstance().defaults.headers.common = headers;
}

function getHeaders() {
  return headers;
}

function getInstance() {
  if (axiosInstance != null) {
    return axiosInstance;
  }
  axiosInstance = axios.create({
    baseURL: import.meta.env.VITE_SOME_KEY,
    // lấy file
    headers: getHeaders(),
  });
  //hook interceptor cài ở đây
  axiosInstance.interceptors.request.use((config) => {
    const token = sessionStorage.getItem("token") || localStorage.getItem("token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
      config.headers.token = token;
      // config.headers["x-api-token"] = token; 
    }
    return config;
  });

  axiosInstance.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (error?.response.data.status === 401 || error?.response.data.status === 1007) {
        localStorage.removeItem("token");
        sessionStorage.removeItem("token");
        window.location.href = "/cms";
      }
      return Promise.reject(error);
    }
  );
  return axiosInstance;
}

function get(endpointApiUrl, payload = {}, config = {}) {
  return getInstance().get(endpointApiUrl, {
    params: payload,
    ...config,
  });
}

function post(endpointApiUrl, payload = {}, config = {}) {
  return getInstance().post(endpointApiUrl, payload, config);
}

function put(endpointApiUrl, payload = {}, config = {}) {
  return getInstance().put(endpointApiUrl, payload, config);
}

function del(endpointApiUrl, payload = {}, config = {}) {
  return getInstance().delete(endpointApiUrl, payload, config);
}

function patch(endpointApiUrl, payload = {}, config = {}) {
  return getInstance().patch(endpointApiUrl, payload, config);
}

export const Axios = {
  axiosInstance,
  getHeaders,
  setHeaders,
  get,
  post,
  put,
  del,
  patch,
};
