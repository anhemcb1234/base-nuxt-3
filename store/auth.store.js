import { defineStore } from "pinia";

export const useAuthStore = defineStore("auth", () => {
  const isAuthenticated = process.client
    ? sessionStorage.getItem("token") || false
    : false;
  return { isAuthenticated };
});
