import { defineStore } from "pinia";

export const layoutStore = defineStore("layout", {
  state: () => ({
    statusLoading: false,
    indexBanner: 0,
    backgroundColor: true,
    isHeight: false,
    isNeedToChangeBg: null,
    isHeaderMobile: false,
    dataNavBar: [
      {
        id: 0,
        nameKr: "엠에스커뮤니케이션",
        linkChild: [
          { id: 1144, name: "about", route: "/ms-communication" },
          { id: 1111, name: "newsroom", route: "/ms-communication-newsroom" },
        ],
      },
      {
        id: 1,
        nameKr: "서비스",
        linkChild: [
          { id: 123, name: "카드형 BPO", route: "/service-card" },
          { id: 121, name: "컨설팅형 BPO", route: "/service-consulting" },
          { id: 1255, name: "매칭형 BPO", route: "/service-matching" },
        ],
      },
      {
        id: 2,
        nameKr: "파트너",
        linkChild: [
          { id: 143, name: "고객사", route: "/partner-client" },
          { id: 113, name: "파트너사", route: "/partner-card" },
        ],
      },
    ],
  }),
  actions: {
    setStatusLoading(payload) {
      this.statusLoading = payload;
    },
    setIndexBanner(index) {
      this.indexBanner = index;
    },
    setIsNeedToChangeBg(payload) {
      this.isNeedToChangeBg = payload;
    },
    setBackgroundColor(payload) {
      this.backgroundColor = payload;
    },
    setIsHeight(payload) {
      this.isHeight = payload;
    },
    setIsHeaderMobile(payload) {
      this.isHeaderMobile = payload;
    },
  },
});
