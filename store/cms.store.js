import { defineStore } from "pinia";

export const useCmsStore = defineStore("cms", {
  state: () => ({
    valueSelectedQuestion: null,
  }),
  actions: {
    setValueSelectedQuestion(index) {
      this.valueSelectedQuestion = index;
    },
  },
});
