import { defineStore } from "pinia";

export const questionStore = defineStore("question", {
  state: () => ({
    stepQuestion: 0,
    valueSelectedQuestion: null,
    additionalInquiriesContent: null,
    statusSubmitForm : false,
    statusLoading: false,
  }),
  actions: {
    setValueSelectedQuestion(index) {
      this.valueSelectedQuestion = index;
    },
    setStepQuestion(payload) {
      this.stepQuestion = payload;
    },
    setAdditionalInquiriesContent(payload) {
      this.additionalInquiriesContent = payload;
    },
    setStatusSubmitForm(payload) {
      this.statusSubmitForm = payload;
    },
    resetState() {
      this.valueSelectedQuestion = null;
      this.stepQuestion = 0;
      this.additionalInquiriesContent = null;
    },
    setStatusLoading(payload) {
      this.statusLoading = payload;
    },
  },
});
