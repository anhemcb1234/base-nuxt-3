import { useService } from "./useService";

export const LoginApi = (payload) => {
  return useService.post(`api/v1/admin/login`, payload);
};

export const useAuthen = {
  LoginApi,
};
