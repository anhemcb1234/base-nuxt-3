import { useService } from "./useService";

const getListQuestions = (payload) => {
  return useService.get(`/public/api/service?page=0&size=9999`, payload);
};

const sendQuestion = (payload) => {
  return useService.post(`/public/api/question`, payload);
};

const getDocumentNewsRoom = (payload) => {
  return useService.get(`/public/api/document?sort=status,createdDate,desc`, payload);
};

const getDocumentNewsRoomID = (payload) => {
  return useService.get(`/public/api/document/${payload}`);
};

const getBlogNewsRoom = (payload) => {
  return useService.get(`/public/api/blog?sort=createdDate,desc`, payload);
};

const getBlogNewsRoomID = (payload) => {
  return useService.get(`/public/api/blog/${payload}`);
};

const getService = (payload) => {
  return useService.get(`/public/api/service`, payload);
};

const getCardBPO = (payload) => {
  return useService.get(`/public/api/service-category/card-bpo?type=2`, payload);
};

const getCardBPOID = (payload) => {
  return useService.get(`/public/api/service/${payload}`);
}

const getMetaSeo = (payload) => {
  return useService.get(`/public/api/page/findByUrl`, payload);
}

export const useClient = {
  getListQuestions,
  sendQuestion,
  getDocumentNewsRoom,
  getBlogNewsRoom,
  getDocumentNewsRoomID,
  getBlogNewsRoomID,
  getService,
  getCardBPO,
  getCardBPOID,
  getMetaSeo
};
