// import { useAuthStore } from "~/store/auth";

export const callService = async (
  endPointUrl,
  method = "POST",
  params,
  headers = null
) => {
  // eslint-disable-next-line no-undef
  // const token = useAuthStore().isAuthenticated
  const token =
    sessionStorage.getItem("token") || localStorage.getItem("token");
  const config = useRuntimeConfig();
  try {
    // eslint-disable-next-line no-undef
    const fetch = await $fetch(
      !params?.urlFullPath ? config.public.apiBase + endPointUrl : endPointUrl,
      {
        onRequest({ options }) {
          // Set the request headers
          options.headers = options.headers || {};
          options.method = method;
          if (method !== "PUT" && method !== "POST") {
            options.query = params;
          } else {
            if (params?.urlFullPath) {
              delete params?.urlFullPath;
            }
            options.body = params;
          }

          if (!headers) {
            options.headers["Content-Type"] = "application/json";
            options.headers.Accept = "application/json";
          } else {
            options.headers["Content-Type"] = "multipart/form-data";
          }

          if (token) {
            options.headers.authorization = `Bearer ${token}`;
            options.headers.token = `${token}`;
          }

          if (headers && headers.token) {
            options.headers.authorization = headers.token;
          }

          if (params?.noToken) {
            options.headers.authorization = "";
          }
          // options.headers['Access-Control-Allow-Origin'] = '*';
        },
        // eslint-disable-next-line no-unused-vars
        onRequestError({ request, options, error }) {},
        // eslint-disable-next-line no-unused-vars
        onResponse({ request, response, options }) {},
        onResponseError(resp) {
          throw resp;
        },
      }
    );

    if (fetch?.error?.value) {
      throw {
        statusCode: fetch?.error?.value?.status,
        message: fetch?.error?.value?.data?.error?.message,
        code: fetch?.error?.value?.data?.error?.code,
      };
    }

    if (typeof fetch === "string") {
      try {
        return JSON.parse(fetch);
      } catch (e) {
        return fetch;
      }
    }

    return fetch;
  } catch (e) {
    console.warn(e);
    throw {
      statusCode: e?.response?.status,
      message:
        e?.response?._data?.error?.message || e?.response?._data?.message,
      code: e?.response?._data?.error?.code || e?.response?._data?.error_code,
      data: e?.response?._data,
    };
  }
};

export const get = async (endPointUrl, params = {}) => {
  return await callService(endPointUrl, "GET", params);
};

export const post = async (endPointUrl, params = {}, headers = null) => {
  return await callService(endPointUrl, "POST", params, headers);
};

export const del = async (endPointUrl, params) => {
  return await callService(endPointUrl, "DELETE", params);
};

export const put = async (endPointUrl, params, headers = null) => {
  return await callService(endPointUrl, "PUT", params, headers);
};

export const useService = {
  get,
  post,
  del,
  put,
};
