import { useService } from "./useService";

const Login = (payload) => {
  return useService.post(`/auth/sign-in`, payload);
};

const getListQuestions = (payload) => {
  return useService.get(`/public/api/question?sort=createdDate,desc`, payload);
};

const detailQuestion = ({ id }) => {
  return useService.get(`/public/api/question/${id}`);
};

const changeQuestion = ({ id, payload }) => {
  return useService.put(`/admin/api/question/${id}`, payload);
};

const getTotalQuestionByDate = () => {
  return useService.get(`/admin/api/question/getTotalQuestionByDate`);
};

const exportExcel = (payload) => {
  return useService.get(
    `/admin/api/question/exportExcelByIds?ids=${payload}`,
    {
      responseType: "blob",
    }
  );
};

export const useCms = {
  Login,
  getListQuestions,
  getTotalQuestionByDate,
  exportExcel,
  detailQuestion,
  changeQuestion,

};
