// useSortableTable.js

export function useSortableTable(initialData, type, statusSort, date, count) {
  if (!type) {
    return;
  }
  let result = initialData.sort((a, b) => {
    const compareValue = date
      ? new Date(a[type]) - new Date(b[type])
      : statusSort
      ? a[type] > b[type]
        ? 1
        : -1
      : b[type] > a[type]
      ? -1
      : 1;

    return statusSort ? compareValue : -compareValue;
  });
  return { result };
}
