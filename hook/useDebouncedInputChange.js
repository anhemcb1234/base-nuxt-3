import { ref } from "vue";
import { useRoute } from "vue-router";

export default function useDebouncedInputChange(delay = 600) {
  const route = useRoute();
  const inputValue = ref("");
  let timeoutId;

  const handleInput = (e) => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }

    timeoutId = setTimeout(() => {
      inputValue.value = e.target.value;
      const newUrl =
        window.location.origin +
        `/${route.name}?q=` +
        encodeURIComponent(inputValue.value);
      window.history.pushState({ path: newUrl }, "", newUrl);
    }, delay);
  };

  return {
    inputValue,
    handleInput,
  };
}
