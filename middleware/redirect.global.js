import { useAuthStore } from "@/store/auth.store";
import { questionStore } from "@/store/question.store";

export default defineNuxtRouteMiddleware((to, from) => {
  const authStore = useAuthStore();
  const store = questionStore();

  store.setValueSelectedQuestion(null);
  // if (!to.path.startsWith("/cms/")) {
  //   console.log("123123123123");
  // }
});
